function bai1() {
  var daySalary = document.getElementById("day-salary").value * 1;
  var workingDays = document.getElementById("working-days").value * 1;
  if (
    daySalary <= 0 ||
    workingDays <= 0 ||
    daySalary == "" ||
    workingDays == "" ||
    isNaN(daySalary) ||
    isNaN(workingDays)
  ) {
    document.getElementById("result-bai-1").innerText = `Lỗi input!`;
  } else {
    document.getElementById("result-bai-1").innerText = daySalary * workingDays;
  }
}

function bai2() {
  var so1 = document.getElementById("so-1").value * 1;
  var so2 = document.getElementById("so-2").value * 1;
  var so3 = document.getElementById("so-3").value * 1;
  var so4 = document.getElementById("so-4").value * 1;
  var so5 = document.getElementById("so-5").value * 1;
  document.getElementById("result-bai-2").innerText =
    (so1 + so2 + so3 + so4 + so5) / 5;
}

function bai3() {
  var currencyInput = document.getElementById("currency").value * 1;
  if (currencyInput <= 0 || currencyInput == "" || isNaN(currencyInput)) {
    document.getElementById("result-bai-3").innerText = `Lỗi input!`;
  } else {
    var rate = currencyInput * 23500;
    formatRate = new Intl.NumberFormat().format(rate);
    document.getElementById("result-bai-3").innerText = formatRate;
  }
}

function bai4() {
  var chieuDai = document.getElementById("chieu-dai").value * 1;
  var chieuRong = document.getElementById("chieu-rong").value * 1;
  var result = "";
  if (
    chieuDai <= 0 ||
    chieuRong <= 0 ||
    chieuDai == "" ||
    chieuRong == "" ||
    isNaN(chieuDai) ||
    isNaN(chieuRong)
  ) {
    result = `Lỗi input!`;
  } else {
    var dienTich = chieuDai * chieuRong;
    var chuVi = (chieuDai + chieuRong) * 2;
    var templateString = `Diện tích: ${dienTich}m2, Chu vi: ${chuVi}m`;
    result = templateString;
  }
  document.getElementById("result-bai-4").innerText = result;
}

// em làm hàm đệ quy
function tinhTongKySoBangDeQuy(number) {
  if (number < 10) {
    return number;
  } else {
    return tinhTongKySoBangDeQuy(Math.floor(number / 10)) + (number % 10);
  }
}

// hàm bai5 em muốn áp dụng đệ quy để xử lý hết được các test case, đệ quy em học ở trường rồi ạ
function bai5() {
  var numberInput = document.getElementById("cac-ky-so").value * 1;
  var result = "";
  if (numberInput > 0 && numberInput.toString().length == 2) {
    result = tinhTongKySoBangDeQuy(numberInput);
  } else {
    result = "Lỗi input!";
  }
  document.getElementById("result-bai-5").innerText = result;
}

function bai5_cachTrenLop() {
  var numberInput = document.getElementById("cac-ky-so").value * 1;
  var result = "";
  if (numberInput > 0 && numberInput.toString().length == 2) {
    var soHangChuc = Math.floor(numberInput / 10);
    var soHangDonVi = numberInput % 10;
    result = soHangChuc + soHangDonVi;
  } else {
    result = "Lỗi input!";
  }
  document.getElementById("result-bai-5").innerText = result;
}

// cách làm này của em: em nghĩ cũng solve dc phần lớn các test case
// idea em: chuyển kiểu dữ liệu số thành string, string trong js được xem như mảng chứa các phần tử là các ký tự (trường hợp này là số), em duyệt qua từng phần tử mảng thì sẽ lấy được từng ký số, sau đó em chuyển kiểu dữ liệu của ký số đó thành số và cộng vào biến total
// em cũng ko biết độ phức tạp về bộ nhớ với thời gian có lớn ko nữa :v
function bai5_cachKhac() {
  var numberInput = document.getElementById("cac-ky-so").value * 1;
  numberInput = numberInput.toString();
  total = 0;
  for (let i = 0; i < numberInput.length; i++) {
    total += parseInt(numberInput[i]);
  }
  document.getElementById("result-bai-5").innerText = total;
}
